\documentclass[aps,notitlepage,12pt]{revtex4-1}
\usepackage{epsfig,graphicx,amssymb,amsmath, amsthm,subeqnarray,rotating,color,float,wrapfig,fancyhdr, setspace,tikz}

\newtheorem{mythm}{Theorem}
\newtheorem{mydef}{Definition}
\newtheorem{myprop}{Proposition}

\def\b{\mathbf}\def\e{\epsilon}

\def\H{\b{H}}
\def\v{\b{v}}
\def\f{\b{f}}

\def\bs{s'}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%My redefined and new commands. If you use something a lot it might be useful to define a new command for it.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\DeclareMathOperator{\sech}{sech}

\newcommand{\setbracket}[1]{\left\{#1 \right\}}
\renewcommand{\Re}[1]{\textrm{Re}(#1)}
\renewcommand{\Im}[1]{\textrm{Im}(#1)}
\renewcommand{\=}[1]{\overline{#1}}
\newcommand{\pder}[2]{\frac{\partial{#1}}{\partial{#2}}}
\newcommand{\der}[2]{\frac{d{#1}}{d{#2}}}
\newcommand{\mC}[1]{\mathcal{#1}}

\def\U{\b{U}}
\newcommand{\cpw}[1]{\textcolor{green}{cpw: #1}}
\newcommand{\tm}[1]{\textcolor{red}{tm: #1}}
\newcommand{\ke}[1]{\textcolor{blue}{ke: #1}}




\def\varepsilon{\epsilon}


\pagestyle{fancy}
\lhead{Kurt Ehlert, Tom Morrell, and Colin Wahl}
%\rhead{University of Wisconsin-Madison}



\begin{document}

\title{A Multiscale Finite Element Method for Elliptic Problems in
Composite Materials and Porous Media}
\author{Kurt Ehlert, Tom Morrell, and Colin Wahl}
\date{\today}

\maketitle

% ABSTRACT?!

\section{Problem Introduction}
Many important problems have multiple spatial scales. For example, groundwater flowing through porous rock, conduction through composite materials, and air turbulence. If the model is detailed enough, there might be very many spatial scales. Solving the associated partial differential equations using a typical finite element method (FEM) is computationally demanding and sometimes infeasible, because the number of finite elements required to resolve the small scales can require an immense amount of memory and computational time. Parallelizing the problem across many CPUs alleviates the time and memory issues, but it does not solve the core problem, and not everyone has access to enough computing resources.

The focus of this paper is to present the multiscale finite element method (MFEM) \cite{hou97}. The goal of the method is to resolve the larger-scale and capture the effects of the small-scale, without actually resolving the small-scale elements. The small-scale information is conveyed to the large-scale through coupling of the global stiffness matrix. In engineering, we do not always need the small-scale detail. We often just need to know the effects of the small-scale on the large scale features. For example, we might need to know the effect of turbulence on an airplane, but we do not need to know all of the details.

To be more specific, MFEM constructs local basis functions for each element, which capture the local information. It gets these basis functions by solving the leading-order homogeneous elliptic equation in each element. We know that in two-scale periodic structures, this approach does indeed converge to the solution \cite{hou99}.

As mentioned earlier, the goal of MFEM is to resolve the large-scale behavior, while reducing memory and CPU time. As for memory, if we let $N$ be the number of elements and let $M$ be the number of subcell elements (in each spatial direction), then the traditional FEM requires $O(M^nN^n)$ memory, where $n$ is the number of dimensions.  This is much more than what MFEM requires, which is $O(M^n + N^n)$ memory. To make this concrete, say $n=2$, $M=128$, and $N = 64$, then the traditional FEM requires over 3000 times as much memory. In practice this could lead to a difference between requiring megabytes and gigabytes.

As for CPU time savings, the two methods are similar if we solve the system only once, although technically MFEM is more expensive. Regular FEM requires $O(N^nM^n)$ operations, whereas MFEM requires $O(N^n + (d-1)N^nM^n)$ operations, where $d$ is the number of nodes in each cell. However, it's important to note that we get a lot of time savings if we solve the system multiple times but with difference forcing functions, because in that case MFEM only requires $O(N^n)$ operations per subsequent run. This leads to huge savings if we are interested in the sensitivity of the system to force perturbations.

Additionally, the reduced memory requirements make parallelization much more effective. For example, the performance of graphics processing units are very sensitive to memory usage. By reducing the memory cost, we can more effectively leverage existing computational technology.

In this paper, we will discuss the details of MFEM, paying particular attention to the problem \begin{equation} \label{main_eqn} -\nabla \cdot a({\bf x}) \nabla u = f \end{equation} on the domain $\Omega = [0,1]^n$ (with $n = 1,2$), where $a$ will frequently have a small length scale of $\varepsilon$. In section II, we present the ``local problem'' and its solution, which gives the basis functions restricted to individual cells. In section III, the results from the local problem are combined to setup the global stiffness matrix and load vector. Once we have these, then we can just use the usual finite element method to get a solution. We call this the ``global problem.''

In section IV, we present our results for some example problems. There is a 1D and a 2D example. For the 1D example, we used a random forcing function, and ran MFEM many times to estimate the expected solution. The 2D example is from \cite{hou97}, and we reproduced their solution. In section V, we discuss the results and point out improvements to the basic MFEM scheme that are also discussed in \cite{hou97}.
%\begin{figure}[htbp]
%\begin{center}
%\includegraphics[width = \textwidth]{CouderFort_2006_from_Bush}
%\caption{(a) Shows the experimental set up where a bouncing droplet is forced through a single-slit and the resulting diffraction. The results of the single-slit experiment are shown in (b) where the dark line shows the distinct regions which heuristically appear to resemble the diffraction pattern of a quantum mechanical system. Similarly, (c) shows the results for double-slit diffraction. This image was adapted from Bush \cite{bush2015} and done without permission. }
%\label{singleslit_diff_exp}
%\end{center}
%\end{figure} 

\section{Local Problem}

To solve the multiscale problem, we have to locally solve a homogeneous elliptic problem in each cell $K$. This decreases the error in the global solution by increasing the local sampling of the coefficients $a(\mathbf{x})$.
%Need to read the paper again and double check that this sentence is legit
Specifically, the local problem that we are required to solve in each cell $K$ is 
\begin{align}\label{original_system}
-\nabla \cdot a(\mathbf{x}) \nabla \phi^i_K &=0,\\
\pder{}{T}a(\mathbf{x}) \pder{}{T} \phi^i_K &= 0 \text{~on~}\partial K,\\
\text{and~}\phi^i_K{\mathbf{x}_j} &= \delta_{i j} 
\end{align}
where $x_j $ are nodal points of $K$ and $T$ is the direction tangent to the boundary up to a sign. 
In particular, if we use square elements, then this implies that $\phi^i_K(\mathbf{x})\biggr\rvert_{\partial K}$ defined piecewise is 
\begin{align*}
\cfrac{\int_\mathbf{x}^{\mathbf{x}_{i+1}} \frac{d\tau}{a(\tau)}}{\int_{\mathbf{x}_i}^{\mathbf{x}_{i+1}} \frac{d\tau}{a(\tau)}}=: g_i(\mathbf{x}),
\end{align*}
after which the PDE takes the familiar form
\begin{align*}
-\nabla \cdot a(\mathbf{x}) \nabla \phi^i_K &=0,\\
\phi^i_K = g_i(\mathbf{x}) &\text{~on~}\partial K\\
\text{and~}\phi^i_K(\mathbf{x}_j) &= \delta_{i j}. 
\end{align*}
If for example we want to solve for $\phi^1_K$ then this system becomes
\begin{align*}
\nabla \cdot a(\mathbf{x}) \nabla \phi^1_K &=0,\\
\phi^1_K = \frac{\int_x^{x_{2}} \frac{d\tau}{a(\tau)}}{\int_{x_1}^{x_{2}} \frac{d\tau}{a(\tau)}} &\text{~on~}\Gamma_1\\
\phi^1_K = \frac{\int_x^{y_{4}} \frac{d\tau}{a(\tau)}}{\int_{y_1}^{y_{4}} \frac{d\tau}{a(\tau)}} &\text{~on~}\Gamma_4\\
\phi^1_K &=0 \text{~on~}\Gamma_{2,3}, 
\end{align*}
where $\Gamma_j$ connects nodes $j$ and $j+1$ for $j = 1,2,3$ and $\Gamma_4$ connects notes $4$ and $1$.
Note that this system is well-posed, so it has a solution.

Our implementation strategy is to solve the original system given by Equation \ref{original_system}. We first wrote a 1D FEM to solve the boundary problem with nonzero Dirichlet boundary conditions. This gave the boundary data for the 2D FEM solver that we wrote for solving the local problems with nonzero Dirichlet boundary conditions. This solver provided the $\phi^i_K$'s, which were used to find the local stiffness matrix and load vector. The stiffness matrix and load vector were subsequently given to the global solver. 

There are other methods for dealing with the boundary data, such as oversampling, that solve this problem in a slightly more efficient manner; however, we chose to implement this scheme as above since we had already developed a 1D solver for implementing MFEM for a 1D problem.

Solving the local problem requires $O((d-1) M^n)$ memory, since we have to solve for $d-1$ local basis functions on $O(M^n)$ nodes. Note that we achieve the $d^\textup{th}$ basis function for free because by linearity, the sum of the basis functions is uniformly $1$ on the entire cell. Since we solve the problem $O(N^n)$ times, the local problem also requires $O((d-1)N^nM^n)$ time.

Once we have the solutions for the $\phi^i_K$'s we can create a local stiffness matrix \[ A_K = (a(\phi^i_K,\phi^j_K))_{ij} \] and local load vector $F_K = ( f, \phi^i_K )_i$. These will be used to solve the global problem in the next section.

\section{Global Problem}

We solve the global problem in the same way as we would solve the ordinary finite element method:  for all the basis functions, we compute the bilinear form $a(\cdot,\cdot)$ on each cell, and then we sum over the cells to get the corresponding entry of the stiffness matrix $A$. We handle the load vector similarly.

For the one-dimensional problem, this amounts to creating a tridiagonal matrix $A$ out of the $A_K$s, overlapping them as in Fig. \ref{1dA}, and summing the corresponding entries to get the global stiffness matrix. The local load vectors $F_K$ are similarly summed to produce a global load vector $F$.
\begin{figure}
	\begin{center}
		\begin{tikzpicture}[scale=1.5]
			\draw (0,0) -- (0,4);
			\draw (0,0) -- (0.4,0);
			\draw (0,4) -- (0.4,4);
			\draw (4,0) -- (4,4);
			\draw (4,0) -- (3.6,0);
			\draw (4,4) -- (3.6,4);
			\draw[dashed] (0.1,3) -- (0.1,3.9);
			\draw[dashed] (0.1,3) -- (1,3);
			\draw[dashed] (1,3.9) -- (0.1,3.9);
			\draw[dashed] (1,3.9) -- (1,3);
			\draw[dashed] (0.5,2.5) -- (0.5,3.5);
			\draw[dashed] (0.5,2.5) -- (1.5,2.5);
			\draw[dashed] (1.5,3.5) -- (0.5,3.5);
			\draw[dashed] (1.5,3.5) -- (1.5,2.5);
			\draw[dashed] (1,2) -- (1,3);
			\draw[dashed] (1,2) -- (2,2);
			\draw[dashed] (2,3) -- (1,3);
			\draw[dashed] (2,3) -- (2,2);
			\draw[dashed] (3,0.1) -- (3,1);
			\draw[dashed] (3,0.1) -- (3.9,0.1);
			\draw[dashed] (3.9,1) -- (3,1);
			\draw[dashed] (3.9,1) -- (3.9,0.1);
			\draw[dotted] (2,2) -- (3,1);
			\node at (0.5,3.5) {$A_1$};
			\node at (1,3) {$A_2$};
			\node at (1.5,2.5) {$A_3$};
			\node at (3.5,0.5) {$A_N$};
			\node[left] at (0,2) {$A = $};
		\end{tikzpicture}
		\caption{A depiction of how the $A_K$'s come together in solving the one-dimensional problem. Overlapping blocks have entries added to get the entries in the stiffness matrix $A$.}
		\label{1dA}
	\end{center}
\end{figure} 

\begin{figure}
	\begin{center}
		\begin{tikzpicture}[scale=3]
			\draw (-0.5,0) -- (3.5,0);
			\draw (-0.5,1) -- (3.5,1);
			\draw (-0.5,2) -- (3.5,2);
			\draw (-0.5,3) -- (3.5,3);
			\draw (0,-0.5) -- (0,3.5);
			\draw (1,-0.5) -- (1,3.5);
			\draw (2,-0.5) -- (2,3.5);
			\draw (3,-0.5) -- (3,3.5);
			\draw (2.33,2) -- (2.33,3);
			\draw (2.67,2) -- (2.67,3);
			\draw (2,2.33) -- (3,2.33);
			\draw (2,2.67) -- (3,2.67);
			\draw[fill=black] (0,0) circle (.15ex);
			\draw[fill=black] (1,0) circle (.15ex);
			\draw[fill=black] (2,0) circle (.15ex);
			\draw[fill=black] (3,0) circle (.15ex);
			\draw[fill=black] (0,1) circle (.15ex);
			\draw[fill=black] (1,1) circle (.15ex);
			\draw[fill=black] (2,1) circle (.15ex);
			\draw[fill=black] (3,1) circle (.15ex);
			\draw[fill=black] (0,2) circle (.15ex);
			\draw[fill=black] (1,2) circle (.15ex);
			\draw[fill=black] (2,2) circle (.15ex);
			\draw[fill=black] (3,2) circle (.15ex);
			\draw[fill=black] (0,3) circle (.15ex);
			\draw[fill=black] (1,3) circle (.15ex);
			\draw[fill=black] (2,3) circle (.15ex);
			\draw[fill=black] (3,3) circle (.15ex);
			\node at (0.5,0.5) {$K_{i+1,j}$};
			\node at (1.5,0.5) {$K_{i+1,j+1}$};
			\node at (0.5,1.5) {$K_{ij}$};
			\node at (1.5,1.5) {$K_{i,j+1}$};
			\node[above left] at (1,1) {$V_{ij}$};
		\end{tikzpicture}
		\caption{The coarse grid on which the large-scale problem is solved. Each vertex $V_{ij}$ is a corner for the four neighboring cells $K_{ij},K_{i,j+1},K_{i+1,j},K_{i+1,j+1}$ on which the local problems (subgrid in upper right-hand corner) are solved.}
		\label{grid}
	\end{center}
\end{figure}

Recalling that $\phi_{K_{ij}}^{p}$ is the $p^\textup{th}$ basis function on cell $K_{ij}$:  i.e., the solution to the local problem on cell $K_{ij}$ which is $1$ at the $p^\textup{th}$ vertex and zero on the others, where $p = 1,2,3,4$ labeled counterclockwise, starting from the bottom left, we note that \[ \phi^{i,j} = \phi_{K_{i,j}}^2 + \phi_{K_{i,j+1}}^1 + \phi_{K_{i+1,j}}^3 + \phi_{K_{i+1,j+1}}^4, \] where we extend the $\phi_K^i$'s so be zero on the other cells and fix the boundary scalings appropriately.

Then we see that the entries of $A$ are obtained by finding the cells $K$ on which both basis functions are nonzero, and then adding up the appropriate entries from the $A_K$. For example, if the $m^\textup{th}$ node is $V_{ij}$, then the $m,m$ entry of $A$ is given by \[ A_{m,m} = A_{K_{i,j}}(2,2) + A_{K_{i,j+1}}(1,1) + A_{K_{i+1,j}}(3,3) + A_{K_{i+1,j+1}}(4,4), \] etc. We construct the load vector $F$ similarly.

Note that the above is easily generalized to other cell-shapes. We could use triangles or any other geometric shapes so long as we have a means of solving the local problem (from the previous section) on the individual cells.

Once we have $A$ and $F$, we solve the linear problem $AU = F$ for the numerical solution, as we would in ordinary FEM. We require constant time and memory to incorporate information from each of the $O(N^n)$ cells into the global stiffness matrix and load vectors, and then the system requires $O(N^2)$ time and memory to write and solve (using a sparse matrix equation solver). Combining this with the complexity estimates from the local problem, we see that MFEM requires $O(N^n+M^n)$ memory and $O(N^n+(d-1)N^nM^n)$ time, making it require less memory than ordinary FEM, but at the cost of being slower by a factor of $M^n$. For this reason, we often limit $M$ to, e.g., $32$, so that the problem can still be solved in a timely manner. This restriction may be eased somewhat on platforms with GPUs or in cluster computing.

\section{Results}

To determine the rate of convergence of our code, we solved (\ref{main_eqn}) in two-dimensions with $a = 1$ and $f = 5 \pi^2 \sin \pi x \sin 2\pi y$. The analytic solution is $u = \sin \pi x \sin 2\pi y$. We tested $N = 3,7,11,16,32$ and $M=16$. The resulting $L^\infty$ errors are reported in Fig. \ref{error_plot}. We see that we have approximately second-order convergence, consistent with the claim in \cite{hou97}.

\begin{figure}[ht]
\begin{center}
\includegraphics[width =0.75\textwidth]{error_plot_N_slope_1p8768}
\caption{The convergence rate to the exact solution by our MFEM implementation when solving \eqref{main_eqn} with $a =1$, $f = 5 \pi ^2 \sin (\pi  x) \sin (2 \pi  y)$. The slope of this log-log plot is 1.8768 which matches well with the predicted second-order convergence.}
\label{error_plot}
\end{center}
\end{figure}

We also solved system (4.1) in \cite{hou97}:  \[ a = \frac{2+P\sin(2\pi x/\varepsilon)}{2 + P\cos(2\pi y/\varepsilon)} + \frac{2+\sin(2\pi y/\varepsilon)}{2+P\sin(2\pi x/\varepsilon)}, ~~~ f = -1, ~~~ u \big|_{\partial \Omega} = 0, \] where $P = 1.8$ and $\varepsilon = 0.04$. The results with $M = 16$ and $N = 16$ can be found in Fig. \ref{their_example}. The solution is characterized by small oscillations in $u$ throughout the medium.

\begin{figure}
\begin{center}
\includegraphics[width =0.75\textwidth]{their_example_16x16}
\caption{Using system (4.1) of \cite{hou97} with $\epsilon = 0.04$, $M = 16$ and $N=16$, we get solutions that appear to be qualitatively similar to those presented in the original paper.}
\label{their_example}
\end{center}
\end{figure}    

We also solved (\ref{main_eqn}) in one dimension with a random forcing function:
\begin{align}\label{example1}
&-\frac{d}{dx}a(x) \frac{d}{dx}u(x) = f(x)\\\nonumber
&a(x) = 15 + 14\cos(250x)\\\nonumber
&f(x) = 10\mathcal{N}(0,1)\\\nonumber
&u(0) = 0, u(1) = 0,
\end{align} where $\mathcal{N}$ refers to a Gaussian distribution. The solutions of this problem from several runs of our code are plotted in Fig. \ref{random_example}.

We recall the advantage of MFEM over FEM that we can reuse our basis functions when running the code multiple times, so later runs are much faster, while still retaining the accuracy of the initial run, since the constancy of $a(\mathbf{x})$ over the several runs means that the stiffness matrix $A$ will remain unchanged. In particular, in later runs, we are using FEM on a grid of size $N = 32$ but have as good of accuracy as if it were run on a grid of size $NM = 32^2$. This enabled us to run several more random forcing functions $f$ than we otherwise would have been able to do using the basic FEM.

\begin{figure}
\begin{center}
\includegraphics[width =0.75\textwidth]{random_forcing_multi_run}
\caption{Using our 1D MFEM to solve \eqref{example1} with a grid given by $M = 32$ and $N=32$. The black line using has a forcing function given by the associated guassian distribution, the magenta line is the average of all the runs, and the red lines are the individual runs.  }
\label{random_example}
\end{center}
\end{figure}    

\section{Discussion}
In this paper, we presented the multiscale finite element method (MFEM). The goal of MFEM is to reduce the computational burden imposed by problems with multiple spatial scales. Its main benefit is a very substantial reduction in memory cost. The savings are problem-dependent, but they can be many orders of magnitude. A problem that normally takes gigabytes of memory could now take mere megabytes. MFEM also saves us a lot of CPU time if we need to change the forcing function a large number of times, such as when we are interested in the solution's sensitivity to the forcing function $f$.

We implemented the MFEM and tested it on a 1D and 2D problem. The 2D problem was taken from \cite{hou97}, and we were able to reproduce their results. We also showed that the method is second-order accurate in space, which is what the analysis in \cite{hou97} suggested.

Many engineering problems involve multiple spatial scales, so it is extremely important to have a method than can solve such problems efficiently. Unfortunately, it does not resolve the small-scale detail, but on the other hand, the small-scale details are typically of a much smaller magnitude than our solution $u$. Mostly, we simply want to accurately estimate the effect of the small-scale on the large-scale. For example, we might just want to know how water flows on a macro-scale, and are not interested in the exact way water flows through small cracks and holes in porous rock.

We did not touch on some of the issues with the basic MFEM scheme, namely resonance effects and oversampling. The resonance effect discussed in \cite{hou97} occurs when the spatial step size $h$ is approximately equal to $\epsilon$, where $\epsilon$ is the size of the small-scale of the problem. When $h \sim \epsilon$, the convergence rate can decreases dramatically, even if the error is still small. To address this issue, \cite{hou97} uses oversampling. To summarize, oversampling solves the local problem on a larger domain, and then we restrict the solution to the original domain.

The MFEM as presented in \cite{hou97} does not address models with multiple time scales. There are many important applications where multiple time scales are in issue, such as biological processes and reaction-diffusion systems. It would be interesting to look into methods than can handle multiple spatial and temporal scales, and see if we can still get substantial memory and CPU time improvements.

MFEM is an important method with many applications, and it demonstrates the flexibility of finite element methods in general. We really learned a lot by reading about and implementing MFEM, and we all think it will be useful knowledge in the future.

\bibliography{master_reference}
\bibliographystyle{plain}







\end{document}
