\documentclass{beamer}
\usepackage{tikz}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Comment out to run if you don't have any packages
\usepackage{beamerinnerthemecircles, beamerouterthemeshadow}
\usepackage{amsmath, amsfonts, amscd, epsfig, amssymb, amsthm}
\usepackage{wrapfig}
\usepackage{tikz}
\usepackage[percent]{overpic}
\usepackage{color}
\usepackage[utf8]{inputenc}
\usepackage{newunicodechar}
\usepackage{graphicx,caption,subcaption, cite}

\mode<presentation> {
    %\usetheme{Frankfurt} 
    %\usetheme{Goettingen}
    %\usetheme{CambridgeUS}
    %\usetheme{Warsaw}
    % ---------------------------------------------------------------------------------
    % Colin's formating changes
    % ---------------------------------------------------------------------------------
    \usetheme{Dresden}
    \usecolortheme{beaver} 
    \usefonttheme{professionalfonts} 
    \setbeamercolor{structure}{fg=darkred}
    \setbeamercolor{block title}{bg=transperent}
    \setbeamercolor{frametitle}{bg=white}
    \setbeamertemplate{frametitle}[default][colsep=-4bp,rounded=false,shadow=false]
    \setbeamertemplate{bibliography item}{\insertbiblabel}
\setbeamertemplate{navigation symbols}{}

        
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title[]{The Multiscale Finite Element Method
	\footnote{T. Hou and X. Wu, ``A Multiscale Finite Element Method for Elliptic Problems in Composite Materials and Porous Media,'' Journal of Computational Physics {\bf 134}, 169-189 (1997).}
}
\author[]{
	\large{Kurt Ehlert, Tom Morrell, Colin Wahl}
}
\institute[]{\large{University of Wisconsin--Madison}}
		
\date[]{\begin{large} 3 May 2016 \end{large}}

\begin{document}

\begin{frame}
   \titlepage
\end{frame}

\begin{frame}{Main Idea}
	Use to solve problems with multiple scales, which require a very large grid to solve
	\begin{itemize}[<+->]
		\item Break up grid into main coarse grid and several smaller grids:
		\begin{center}
		\begin{tikzpicture}
			\draw (-0.5,0) -- (3.5,0);
			\draw (-0.5,1) -- (3.5,1);
			\draw (-0.5,2) -- (3.5,2);
			\draw (-0.5,3) -- (3.5,3);
			\draw (0,-0.5) -- (0,3.5);
			\draw (1,-0.5) -- (1,3.5);
			\draw (2,-0.5) -- (2,3.5);
			\draw (3,-0.5) -- (3,3.5);
			% \pause
			\draw (1.33,1) -- (1.33,2);
			\draw (1.67,1) -- (1.67,2);
			\draw (1,1.33) -- (2,1.33);
			\draw (1,1.67) -- (2,1.67);
		\end{tikzpicture}
		\end{center}
		\item Solve local problem on smaller local grids, then put data together to solve problem on main coarse grid
	\end{itemize}
\end{frame}

\begin{frame}{What's the Point?}
	\begin{itemize}[<+->]
		\item Memory Management:
		\begin{itemize}
			\item Let $N$ be number of elements in each direction on coarse grid, $M$ corresponding value for smaller local grids
			\item Solving the problem without the multiscale FEM requires $O(N^nM^n)$ memory, where $n$ is the number of dimensions
			\item The multiscale FEM only requires $O(M^n+N^n)$ memory
			\item For example, if $n = 2, M = 32, N = 512$, LFEM requires 49GB memory, but MFEM only requires 48MB!
		\end{itemize}
		\item Efficient Parallelization:
		\begin{itemize}
			\item Easy to allocate subproblems to separate processors, e.g., in a GPU or cluster
		\end{itemize}
		\item Not much more expensive for single runs, and computationally more efficient for multiple runs ($O(N^n)$ on later runs of MFEM vs $O(N^nM^n)$ for LFEM)
	\end{itemize}
\end{frame}

\begin{frame}{Global Problem}
	\begin{center}
		\begin{tikzpicture}
		\draw (-0.5,0) -- (3.5,0);
		\draw (-0.5,1) -- (3.5,1);
		\draw (-0.5,2) -- (3.5,2);
		\draw (-0.5,3) -- (3.5,3);
		\draw (0,-0.5) -- (0,3.5);
		\draw (1,-0.5) -- (1,3.5);
		\draw (2,-0.5) -- (2,3.5);
		\draw (3,-0.5) -- (3,3.5);
		\draw (1.33,1) -- (1.33,2);
		\draw (1.67,1) -- (1.67,2);
		\draw (1,1.33) -- (2,1.33);
		\draw (1,1.67) -- (2,1.67);
		\draw[fill=black] (0,0) circle (.5ex);
		\draw[fill=black] (1,0) circle (.5ex);
		\draw[fill=black] (2,0) circle (.5ex);
		\draw[fill=black] (3,0) circle (.5ex);
		\draw[fill=black] (0,1) circle (.5ex);
		\draw[fill=black] (1,1) circle (.5ex);
		\draw[fill=black] (2,1) circle (.5ex);
		\draw[fill=black] (3,1) circle (.5ex);
		\draw[fill=black] (0,2) circle (.5ex);
		\draw[fill=black] (1,2) circle (.5ex);
		\draw[fill=black] (2,2) circle (.5ex);
		\draw[fill=black] (3,2) circle (.5ex);
		\draw[fill=black] (0,3) circle (.5ex);
		\draw[fill=black] (1,3) circle (.5ex);
		\draw[fill=black] (2,3) circle (.5ex);
		\draw[fill=black] (3,3) circle (.5ex);
		\end{tikzpicture}
	\end{center}
	\begin{itemize}[<+->]
		\item Solve $A\xi = F$ on the coarse as we would normally do when performing the FEM
		\item But now, instead of being piecewise linear, we solve for basis functions using $a(\cdot)$ and $f(\cdot)$ on the smaller local grids
	\end{itemize}
\end{frame}

\begin{frame}{Local Subproblem}
	\begin{center}
		\begin{tikzpicture}[scale=0.9]
		\draw (-0.5,0) -- (3.5,0);
		\draw (-0.5,1) -- (3.5,1);
		\draw (-0.5,2) -- (3.5,2);
		\draw (-0.5,3) -- (3.5,3);
		\draw (0,-0.5) -- (0,3.5);
		\draw (1,-0.5) -- (1,3.5);
		\draw (2,-0.5) -- (2,3.5);
		\draw (3,-0.5) -- (3,3.5);
		\draw (1.33,1) -- (1.33,2);
		\draw (1.67,1) -- (1.67,2);
		\draw (1,1.33) -- (2,1.33);
		\draw (1,1.67) -- (2,1.67);
		\draw[fill=black] (1,1) circle (.3ex);
		\draw[fill=black] (1.33,1) circle (.3ex);
		\draw[fill=black] (1.67,1) circle (.3ex);
		\draw[fill=black] (2,1) circle (.3ex);
		\draw[fill=black] (1,1.33) circle (.3ex);
		\draw[fill=black] (1.33,1.33) circle (.3ex);
		\draw[fill=black] (1.67,1.33) circle (.3ex);
		\draw[fill=black] (2,1.33) circle (.3ex);
		\draw[fill=black] (1,1.67) circle (.3ex);
		\draw[fill=black] (1.33,1.67) circle (.3ex);
		\draw[fill=black] (1.67,1.67) circle (.3ex);
		\draw[fill=black] (2,1.67) circle (.3ex);
		\draw[fill=black] (1,2) circle (.3ex);
		\draw[fill=black] (1.33,2) circle (.3ex);
		\draw[fill=black] (1.67,2) circle (.3ex);
		\draw[fill=black] (2,2) circle (.3ex);
		\end{tikzpicture}
	\end{center}
	\vskip -6pt
	\begin{itemize}[<+->]
		\item Solve $-\nabla \cdot a({\bf x}) \nabla \phi = 0$ on local grid
		\item Boundary conditions:
		 \begin{itemize}
		 	\item 1 on one corner and 0 on the others (have to solve 4 times for above grid)
		 	\item Solve $\frac{\partial}{\partial x_i} a({\bf x}) \frac{\partial \phi(x_i)\big|_{\partial K}}{\partial x_i} = 0$ for edge nodes
		 	\item Can try oversampling to avoid solving ODE on boundary:  $\phi$ linear on each boundary of slightly larger grid
		 \end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Example}
\begin{figure}[htbp]
\centering
\includegraphics[width=0.65\textwidth]{random_forcing_1D} 
\end{figure}

\begin{itemize}
\item $\frac{d}{dx} a(x) \frac{d}{dx} u(x) = f(x)$
\item $a(x) = 15+14*\cos(x*250)$
\item $f(x) = 10*\mathcal{N}(0,1)$
\item $u(0)=0$ and $u(1) = 0$
\end{itemize}
\end{frame}


\end{document}
