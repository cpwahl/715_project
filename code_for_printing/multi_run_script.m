
N = 32;
M = 32;
A = zeros(N-1,N-1);
F = zeros(N-1,1);

x_grid= linspace(0,1,N+1);

a=@(x)(15+14.*cos(x*250));
%a = @(x)(rand([length(x) 1]));
%f= @(x)(tanh(x.^6)+1);

num_runs = 10;

u = zeros([N+1 num_runs+1]);

%u = zeros([N+1 num_runs]);

for j = 1:num_runs
xi_L = local_basis(a, 1, 0, x_grid);
xi_R = local_basis(a, 0, 1, x_grid);

f = @(x)(10*randn([length(x) 1]));
[lA,lF] = local_stiff_load_multi_run(a,f,0,1/N,M, xi_L, xi_R);
A(1,1) = lA(2,2);
F(1) = lF(2);
[lA,lF] = local_stiff_load_multi_run(a,f,(N-1)/N,1,M, xi_L, xi_R);
A(N-1,N-1) = lA(1,1);
F(N-1) = lF(1);
for i=1:N-2
    [lA,lF] = local_stiff_load_multi_run(a,f,i/N,(i+1)/N,M, xi_L, xi_R);
    A(i:(i+1),i:(i+1)) = A(i:(i+1),i:(i+1)) + lA;
    F(i:(i+1)) = F(i:(i+1)) + lF;
end

u(2:end-1,j) = A\F;


end

f = @(x)(10*1/sqrt(2*pi).*exp(-1/2*x.^2));
[lA,lF] = local_stiff_load_multi_run(a,f,0,1/N,M, xi_L, xi_R);
A(1,1) = lA(2,2);
F(1) = lF(2);
[lA,lF] = local_stiff_load_multi_run(a,f,(N-1)/N,1,M, xi_L, xi_R);
A(N-1,N-1) = lA(1,1);
F(N-1) = lF(1);
for i=1:N-2
    [lA,lF] = local_stiff_load_multi_run(a,f,i/N,(i+1)/N,M, xi_L, xi_R);
    A(i:(i+1),i:(i+1)) = A(i:(i+1),i:(i+1)) + lA;
    F(i:(i+1)) = F(i:(i+1)) + lF;
end

u(2:end-1,end) = A\F;

u_avg = transpose(1/num_runs.*sum(transpose(u(:,1:end))));

plot(x_grid, u_avg, 'm-*')
hold on
%plot(x_grid, u)

plot(transpose(x_grid), u(:,1:end-1), 'r-')
plot(transpose(x_grid), u(:,end), 'k-*')