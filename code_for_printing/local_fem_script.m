%Script that will eventually be turned into the local finite element method
clear
%Parameters that are adjustable and might be inputs of the function
%eventually
epsilon = 0.6;
phi_L = 0;
phi_R = 1;
%a = @(x)(cos(x/epsilon)+2);
a= @(x)(cos(x));

grid_points = 5;

x_grid = linspace(0,1,grid_points);


M =zeros([grid_points grid_points]);



for j = 1:grid_points-1
    
    %Should actually change this integration to trapezoidal rule in the
    %future so that a(x) doesn't need to be specified as a function.
    M_local(1,1) = integral(@(x)(a(x).*D_linear_L(x,x_grid(j),x_grid(j+1)).*D_linear_L(x,x_grid(j),x_grid(j+1))), x_grid(j), x_grid(j+1));
    M_local(2,2) = integral(@(x)(a(x).*D_linear_R(x,x_grid(j),x_grid(j+1)).*D_linear_R(x,x_grid(j),x_grid(j+1))), x_grid(j), x_grid(j+1));
    M_local(1,2) = integral(@(x)(a(x).*D_linear_R(x,x_grid(j),x_grid(j+1)).*D_linear_L(x,x_grid(j),x_grid(j+1))), x_grid(j), x_grid(j+1));
    M_local(2,1) = integral(@(x)(a(x).*D_linear_L(x,x_grid(j),x_grid(j+1)).*D_linear_R(x,x_grid(j),x_grid(j+1))), x_grid(j), x_grid(j+1));
    
    % Constructs the global stiffness matrix from the local stiffness
    % matrix
    M(j:j+1, j:j+1) = M(j:j+1, j:j+1)+M_local;
    
end

M_global = M(2:end-1, 2:end-1);

F(1) =  -integral(@(x)(a(x).*D_linear_R(x,x_grid(1),x_grid(2)).*D_linear_L(x,x_grid(1),x_grid(2))), x_grid(1), x_grid(2))*phi_L;
F(grid_points-2) = -integral(@(x)(a(x).*D_linear_R(x,x_grid(end-1),x_grid(end)).*D_linear_L(x,x_grid(end-1),x_grid(end))), x_grid(end-1), x_grid(end))*phi_R;

xi = M_global\transpose(F);




