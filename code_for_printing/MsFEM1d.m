N = 32;
M = 32;
A = zeros(N-1,N-1);
F = zeros(N-1,1);

x_grid= linspace(0,1,M+1);

a=@(x)(15+14*cos(x*250));
%a = @(x)(rand([length(x) 1]));
%f= @(x)(tanh(x.^6)+1);
f = @(x)(10*randn([length(x) 1]));

[lA,lF] = local_stiff_load(a,f,0,1/N,M);
A(1,1) = lA(2,2);
F(1) = lF(2);
[lA,lF] = local_stiff_load(a,f,(N-1)/N,1,M);
A(N-1,N-1) = lA(1,1);
F(N-1) = lF(1);
for i=1:N-2
    [lA,lF] = local_stiff_load(a,f,i/N,(i+1)/N,M);
    A(i:(i+1),i:(i+1)) = A(i:(i+1),i:(i+1)) + lA;
    F(i:(i+1)) = F(i:(i+1)) + lF;
end

u = A\F;
u = [0;u;0]
plot(transpose(x_grid), u)