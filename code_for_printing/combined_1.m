function a = compute_a(x,eps)
% a = a(x,eps) from eq. (4.1)
% x is 2x1 position vector, 0 < eps << 1 small parameter

ss = sin(2*pi*x/eps);
cc = cos(2*pi*x/eps);

a = (2+1.8*ss(1,:))./(2+1.8*cc(2,:)) + (2+ss(2,:))./(2+1.8*ss(1,:));

end

function out = D_linear_L(x,xl,xr)
    out = 1/(xr-xl);
end

function out = D_linear_lower_L(x, y, xL, xR, yL, yR)
    out = [(y - yR)/((xL - xR)*(yL - yR)); (x - xR)/((xL - xR)*(yL - yR))];
end

function out = D_linear_lower_R(x, y, xL, xR, yL, yR)
    out = [(y - yR)/((xL - xR)*(-yL + yR)); (x - xL)/((xL - xR)*(-yL + yR))];
end

function out = D_linear_R(x,xl,xr)
    out = 1/(xl-xr);
end

function out = D_linear_upper_L(x, y, xL, xR, yL, yR)
    out = [(y - yL)/((-xL + xR)*(yL - yR)); (x - xR)/((-xL + xR)*(yL - yR))];
end

function out = D_linear_upper_R(x, y, xL, xR, yL, yR)
    out = [(y - yL)/((xL - xR)*(yL - yR)); (x - xL)/((xL - xR)*(yL - yR))];
end
