function out = linear_L_plotting(x,xl,xr)
if x<=xr && x>=xl
    out = (x-xl)/(xr-xl);;
else
    out =0;
end
end

function out = linear_L(x,xl,xr)
    out = (x-xl)/(xr-xl);
end

function out = linear_R_plotting(x,xl,xr)
if x<=xr && x>=xl
    out = (x-xr)/(xl-xr);
else
    out =0;
end
end

function out = linear_R(x,xl,xr)
    out = (x-xr)/(xl-xr);
end