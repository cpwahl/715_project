% MsFEM.m

% solves -D . aDu = f on [0,1]^2, where D is gradient
% a(x), f = -1, boundary conditions (u = 0 on boundary) given in (4.1)

N = 32;
h = 1/(N+1);
lS = cell(N+1,N+1,2); % localStiffness

for i=1:(N+1)
    for j=1:(N+1)
        % local(i,j) computes stiffness matrix on cell located at
        % [(i-1)*h,i*h]x[(j-1)*h,j*h]
        [A,F] = local(i,j,h);
        lS{i,j,1} = A; % A should be 4x4, labeled counterclockwise from bottom left
        lS(i,j,2} = F; % F should be 4x1
    end
end


coarseA = zeros(N^2,N^2);
coarseF = zeros(N^2,1);


for k=1:N^2
    i = ceil(k/N);
    j = (k-i*N)+N;
    coarseA(k,k) = lS{i,j,1}(2,2)+lS{i,j+1,1}(3,3)...
                    +lS{i+1,j,1}(1,1)+lS{i+1,j+1,1}(4,4);
    if j ~= 1
        coarseA(k,k-1) = lS{i,j,1}(1,2)+lS{i+1,j,1}(3,4);
        if i ~= 1
            coarseA(k,k-N-1) = lS{i,j,1}(2,4);
        end
        if i ~= N
            coarseA(k,k+N-1) = lS{i+1,j,1}(1,3);
        end
    end
    if j ~= N
        coarseA(k,k+1) = lS{i,j+1,1}(1,2)+lS{i+1,j+1,1}(3,4);
        if i ~= 1
            coarseA(k,k-N+1) = lS{i,j+1,1}(1,3);
        end
        if i ~= N
            coarseA(k,k+N+1) = lS{i+1,j+1,1}(2,4);
        end
    end
    if i ~= 1
        coarseA(k,k-N) = lS{i,j,1}(2,3)+lS{i,j+1,1}(1,4);
    end
    if i ~= N
        coarseA(k,k+N) = lS{i+1,j,1}(2,3)+lS{i+1,j+1,1}(1,4);
    end
    
    coarseF(k) = lS{i,j,2}(2)+lS{i+1,j,2}(3)...
                  +lS{i,j+1,2}(1)+lS{i+1,j+1,2}(4);
    % need to also consider boundary terms in general, but with Dirichlet
    % BCs, there is no need for a correction here
end

U = coarseA\coarseF;

