%Script that will eventually be turned into the local finite element method
clear
%Parameters that are adjustable and might be inputs of the function
%eventually
a= @(x,y)(cos(x.*10).*cos(y.*10));

grid_points = N+2;
local_grid_points=M+2;

% grid_points = 5;
% local_grid_points = 5;
% N = grid_points-2;


x_global_grid = linspace(0,1,grid_points);
y_global_grid = linspace(0,1,grid_points);

delta_x = 1/(grid_points-1);
delta_y = 1/(grid_points-1);



xi_1= zeros(grid_points);
xi_2= zeros(grid_points);
xi_3= zeros(grid_points);
xi_4= zeros(grid_points);


for j = 1:grid_points-1
    
    
    for m = 1:grid_points-1
        
        [xi_1, x_local_grid, y_local_grid] = local_basis_2d(a, x_global_grid(j),x_global_grid(j+1),y_global_grid(j),x_global_grid(j+1), local_grid_points, 'LLC');
        xi_2 = local_basis_2d(a,  x_global_grid(j),x_global_grid(j+1),y_global_grid(j),x_global_grid(j+1), local_grid_points, 'LRC');
        xi_3 = local_basis_2d(a,  x_global_grid(j),x_global_grid(j+1),y_global_grid(j),x_global_grid(j+1), local_grid_points, 'URC');
        xi_4 = local_basis_2d(a,  x_global_grid(j),x_global_grid(j+1),y_global_grid(j),x_global_grid(j+1), local_grid_points, 'ULC');
        
%         xi_1(2:end-1, 2:end-1) = transpose(reshape(xi_1_temp ,[N N]));
%         xi_2(2:end-1, 2:end-1) = transpose(reshape(xi_2_temp ,[N N]));
%         xi_3(2:end-1, 2:end-1) = transpose(reshape(xi_3_temp ,[N N]));
%         xi_4(2:end-1, 2:end-1) = transpose(reshape(xi_4_temp ,[N N]));
%         
%         xi_1(:,1)= boundary_1.lB;
%         xi_1(:,end)= boundary_1.rB;
%         xi_1(1, 2:end-1)= boundary_1.uB;
%         xi_1(end, 2:end-1)= boundary_1.dB;
%         
%         xi_2(:,1)= boundary_2.lB;
%         xi_2(:,end)= boundary_2.rB;
%         xi_2(1, 2:end-1)= boundary_2.uB;
%         xi_2(end, 2:end-1)= boundary_2.dB;
        
        
        
        [X_local, Y_local] = meshgrid(x_local_grid, y_local_grid);
        
        a_evaluated = a(X_local, Y_local);
        %M_local(1,1) = integral2(@(x,y)(a(x,y).*fun_11(x,y, x_grid(j), x_grid(j+1), y_grid(j), y_grid(j+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        
        M_local(1,1) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_1.*xi_1));
        M_local(2,2) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_2.*xi_2));
        M_local(3,3) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_3.*xi_3));
        M_local(4,4) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_4.*xi_4));
        
        M_local(1,2) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_1.*xi_2));
        M_local(1,3) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_1.*xi_3));
        M_local(1,4) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_1.*xi_4));
        
        M_local(2,3) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_2.*xi_3));
        M_local(2,4) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_2.*xi_4));
        
        M_local(3,4) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_3.*xi_4));
        
        M_local(2,1) = M_local(1,2);
        M_local(3,1) = M_local(1,3);
        M_local(4,1) = M_local(1,4);
        M_local(3,2) = M_local(2,3);
        M_local(4,2) = M_local(2,4);
        M_local(4,3) = M_local(3,4);
        
        
        F_local(1) = trapz(y_local_grid, trapz(x_local_grid, f_evaluated.*xi_1));
        F_local(2) = trapz(y_local_grid, trapz(x_local_grid, f_evaluated.*xi_2));
        F_local(3) = trapz(y_local_grid, trapz(x_local_grid, f_evaluated.*xi_3));
        F_local(4) = trapz(y_local_grid, trapz(x_local_grid, f_evaluated.*xi_4));
        
        
    end

    
end



