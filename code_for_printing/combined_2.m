function out = fun_11(x,y,xL,xR,yD,yU)
out = ((x - xR).^2 + (y - yU).^2)/((xL - xR).^2*(yD - yU).^2);
end

function out = fun_12(x,y,xL,xR,yD,yU)
out = -((x.^2+xL*xR-x.*(xL+xR)+(y-yU).^2)/((xL-xR).^2*(yD-yU).^2));
end

function out = fun_13(x,y,xL,xR,yD,yU)
out = (x.^2+xL.*xR-x.*(xL+xR)+(y-yD).*(y-yU))/((xL-xR).^2*(yD-yU).^2);
end

function out = fun_14(x,y,xL,xR,yD,yU)
out = -(((x-xR).^2+(y-yD).*(y-yU))/((xL-xR).^2*(yD-yU).^2));
end

function out = fun_22(x,y,xL,xR,yD,yU)
out = ((x-xL).^2+(y-yU).^2)/((xL-xR)^2*(yD-yU)^2);
end

function out = fun_23(x,y,xL,xR,yD,yU)
out = -(((x-xL).^2+(y-yD).*(y-yU))/((xL-xR)^2*(yD-yU)^2));
end

function out = fun_24(x,y,xL,xR,yD,yU)
out = (x.^2+xL*xR-x.*(xL+xR)+(y-yD).*(y-yU))/((xL-xR)^2*(yD-yU)^2);
end

function out = fun_33(x,y,xL,xR,yU,yD)
out = ((x-xL).^2+(y-yD).^2)/((xL-xR)^2*(yD-yU)^2);
end

function out = fun_34(x,y,xL,xR,yD,yU)
out = -((x.^2+xL.*xR-x.*(xL+xR)+(y-yD).^2)/((xL-xR)^2*(yD-yU)^2));
end

function out = fun_44(x,y,xL,xR,yD,yU)
out = ((x-xR).^2+(y-yD).^2)/((xL-xR)^2*(yD-yU)^2);
end