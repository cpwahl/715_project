clear

x_L = 0;
x_R = 1;
y_L = 0;
y_R = 1;

a= @(x)(cos(x));
f= @(x)(1+2.*x+3.*x.^2);
grid_points = 101;

x_grid = linspace(x_L, x_R, x_grid_points);
y_grid = linspace(y_L, y_R, x_grid_points);

a_evaluated = a(transpose(x_grid));
f_evaluated = f(transpose(x_grid));

%xi = local_basis(a, phi_L, phi_R, x_L, x_R, grid_points)
xi_L = local_basis(a, 1, 0, x_grid);
xi_R = local_basis(a, 0, 1, x_grid);

delta_x = (x_L - x_R)/x_grid_points;
delta_y = (y_L - y_R)/x_grid_points;


%Computes the centered difference except for the left and right end points
%which use left and right sided derivatives. This could be causing some
%problems and first order accuracy but I am not sure what the best way to
%fix this would be.
xi_L_prime = 1/(2*delta_x).*(xi_L(1:end-2)-xi_L(3:end)); 
xi_R_prime = 1/(2*delta_x).*(xi_R(1:end-2)-xi_R(3:end));
% xi_L_prime = [ 1/delta_x.*(xi_L(1) - xi_L(2)); xi_L_prime; 1/delta_x.*(xi_L(end-1) - xi_L(end))];
% xi_R_prime = [ 1/delta_x.*(xi_R(1) - xi_R(2)); xi_R_prime; 1/delta_x.*(xi_R(end-1) - xi_R(end))];
%Using left and right sided derivatives substantially decreased the rate of
%convergence so I just did the analytic derivative approaching from the
%left and right which helped a lot.
xi_L_prime = [ -delta_x.*(xi_L(1)); xi_L_prime; -delta_x.*xi_L(end)];
xi_R_prime = [ delta_x.*xi_R(1) ; xi_R_prime; delta_x.*xi_R(end)];


Stiff_local = NaN([2 2]);

Stiff_local(1,1) = trapz(x_grid, a_evaluated.*xi_L_prime.*xi_L_prime);
Stiff_local(1,2) = trapz(x_grid, a_evaluated.*xi_R_prime.*xi_L_prime);
Stiff_local(2,1) = Stiff_local(1,2);
Stiff_local(2,2) = trapz(x_grid, a_evaluated.*xi_R_prime.*xi_R_prime);

Load_local(1, :) = trapz(x_grid, f_evaluated.*xi_L);
Load_local(2, :) = trapz(x_grid, f_evaluated.*xi_R);