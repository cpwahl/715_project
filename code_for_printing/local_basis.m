function [out] = local_basis(a, phi_L, phi_R, x_grid)

array_valued_option = false;

grid_points = length(x_grid);

M =zeros([grid_points grid_points]);



for j = 1:grid_points-1
    
    x_sub_grid = linspace(x_grid(j), x_grid(j+1), 5);
    
    %Should actually change this integration to trapezoidal rule in the
    %future so that a(x) doesn't need to be specified as a function.
%     M_local(1,1) = integral(@(x)(a(x).*D_linear_L(x,x_grid(j),x_grid(j+1)).*D_linear_L(x,x_grid(j),x_grid(j+1))), x_grid(j), x_grid(j+1), 'ArrayValued', array_valued_option);
%     M_local(2,2) = integral(@(x)(a(x).*D_linear_R(x,x_grid(j),x_grid(j+1)).*D_linear_R(x,x_grid(j),x_grid(j+1))), x_grid(j), x_grid(j+1), 'ArrayValued', array_valued_option);
%     M_local(1,2) = integral(@(x)(a(x).*D_linear_R(x,x_grid(j),x_grid(j+1)).*D_linear_L(x,x_grid(j),x_grid(j+1))), x_grid(j), x_grid(j+1), 'ArrayValued', array_valued_option);
%     M_local(2,1) = integral(@(x)(a(x).*D_linear_L(x,x_grid(j),x_grid(j+1)).*D_linear_R(x,x_grid(j),x_grid(j+1))), x_grid(j), x_grid(j+1), 'ArrayValued', array_valued_option);
%     
    M_local(1,1) = trapz(x_sub_grid, a(x_sub_grid).*D_linear_L(x_sub_grid,x_grid(j),x_grid(j+1)).*D_linear_L(x_sub_grid,x_grid(j),x_grid(j+1)));
    M_local(2,2) = trapz(x_sub_grid, a(x_sub_grid).*D_linear_R(x_sub_grid,x_grid(j),x_grid(j+1)).*D_linear_R(x_sub_grid,x_grid(j),x_grid(j+1)));
    M_local(1,2) = trapz(x_sub_grid, a(x_sub_grid).*D_linear_R(x_sub_grid,x_grid(j),x_grid(j+1)).*D_linear_L(x_sub_grid,x_grid(j),x_grid(j+1)));
    M_local(2,1) = M_local(1,2);
    
    % Constructs the global stiffness matrix from the local stiffness
    % matrix
    M(j:j+1, j:j+1) = M(j:j+1, j:j+1)+M_local;
    
end

M_global = M(2:end-1, 2:end-1);

F(1) =  -integral(@(x)(a(x).*D_linear_R(x,x_grid(1),x_grid(2)).*D_linear_L(x,x_grid(1),x_grid(2))), x_grid(1), x_grid(2),'ArrayValued', array_valued_option)*phi_L;
F(grid_points-2) = -integral(@(x)(a(x).*D_linear_R(x,x_grid(end-1),x_grid(end)).*D_linear_L(x,x_grid(end-1),x_grid(end))), x_grid(end-1), x_grid(end), 'ArrayValued', array_valued_option)*phi_R;

xi = M_global\transpose(F);

xi = [phi_L; xi; phi_R];
out = xi;


