function out = fun_33(x,y,xL,xR,yU,yD)
out = ((x-xL).^2+(y-yD).^2)/((xL-xR)^2*(yD-yU)^2);
end