function a = compute_a(x,eps)
% a = a(x,eps) from eq. (4.1)
% x is 2x1 position vector, 0 < eps << 1 small parameter

ss = sin(2*pi*x/eps);
cc = cos(2*pi*x/eps);

a = (2+1.8*ss(1,:))./(2+1.8*cc(2,:)) + (2+ss(2,:))./(2+1.8*ss(1,:));

end
