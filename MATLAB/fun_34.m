function out = fun_34(x,y,xL,xR,yD,yU)
out = -((x.^2+xL.*xR-x.*(xL+xR)+(y-yD).^2)/((xL-xR)^2*(yD-yU)^2));
end