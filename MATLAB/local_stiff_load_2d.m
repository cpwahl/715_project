function [M_local, F_local] = local_stiff_load_2d(a, f, M, x_global_grid, y_global_grid, n, m)

local_grid_points = M+2;

[xi_1, xi_1_prime_x, xi_1_prime_y, x_local_grid, y_local_grid] = local_basis_2d(a, x_global_grid(n),x_global_grid(n+1),y_global_grid(m),x_global_grid(m+1), local_grid_points, 'LLC');
[xi_2, xi_2_prime_x, xi_2_prime_y] = local_basis_2d(a,  x_global_grid(n),x_global_grid(n+1),y_global_grid(m),x_global_grid(m+1), local_grid_points, 'LRC');
[xi_3, xi_3_prime_x, xi_3_prime_y] = local_basis_2d(a,  x_global_grid(n),x_global_grid(n+1),y_global_grid(m),x_global_grid(m+1), local_grid_points, 'URC');
[xi_4, xi_4_prime_x, xi_4_prime_y] = local_basis_2d(a,  x_global_grid(n),x_global_grid(n+1),y_global_grid(m),x_global_grid(m+1), local_grid_points, 'ULC');


[X_local, Y_local] = meshgrid(x_local_grid, y_local_grid);

a_evaluated = a(X_local, Y_local);
f_evaluated = f(X_local, Y_local);

% M_local(1,1) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_1.*xi_1));
% M_local(2,2) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_2.*xi_2));
% M_local(3,3) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_3.*xi_3));
% M_local(4,4) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_4.*xi_4));
% 
% M_local(1,2) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_1.*xi_2));
% M_local(1,3) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_1.*xi_3));
% M_local(1,4) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_1.*xi_4));
% 
% M_local(2,3) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_2.*xi_3));
% M_local(2,4) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_2.*xi_4));
% 
% M_local(3,4) = trapz(y_local_grid, trapz(x_local_grid, a_evaluated.*xi_3.*xi_4));

M_local(1,1) = trapz(x_local_grid, trapz(y_local_grid, a_evaluated.*(xi_1_prime_x.*xi_1_prime_x + xi_1_prime_y.*xi_1_prime_y)));
M_local(2,2) = trapz(x_local_grid, trapz(y_local_grid, a_evaluated.*(xi_2_prime_x.*xi_2_prime_x + xi_2_prime_y.*xi_2_prime_y)));
M_local(3,3) = trapz(x_local_grid, trapz(y_local_grid, a_evaluated.*(xi_3_prime_x.*xi_3_prime_x + xi_3_prime_y.*xi_3_prime_y)));
M_local(4,4) = trapz(x_local_grid, trapz(y_local_grid, a_evaluated.*(xi_4_prime_x.*xi_4_prime_x + xi_4_prime_y.*xi_4_prime_y)));

M_local(1,2) = trapz(x_local_grid, trapz(y_local_grid, a_evaluated.*(xi_1_prime_x.*xi_2_prime_x + xi_1_prime_y.*xi_2_prime_y)));
M_local(1,3) = trapz(x_local_grid, trapz(y_local_grid, a_evaluated.*(xi_1_prime_x.*xi_3_prime_x + xi_1_prime_y.*xi_3_prime_y)));
M_local(1,4) = trapz(x_local_grid, trapz(y_local_grid, a_evaluated.*(xi_1_prime_x.*xi_4_prime_x + xi_1_prime_y.*xi_4_prime_y)));

M_local(2,3) = trapz(x_local_grid, trapz(y_local_grid, a_evaluated.*(xi_2_prime_x.*xi_3_prime_x + xi_2_prime_y.*xi_3_prime_y)));
M_local(2,4) = trapz(x_local_grid, trapz(y_local_grid, a_evaluated.*(xi_2_prime_x.*xi_4_prime_x + xi_2_prime_y.*xi_4_prime_y)));

M_local(3,4) = trapz(x_local_grid, trapz(y_local_grid, a_evaluated.*(xi_3_prime_x.*xi_4_prime_x + xi_3_prime_y.*xi_4_prime_y)));

M_local(2,1) = M_local(1,2);
M_local(3,1) = M_local(1,3);
M_local(4,1) = M_local(1,4);
M_local(3,2) = M_local(2,3);
M_local(4,2) = M_local(2,4);
M_local(4,3) = M_local(3,4);


F_local(1) = trapz(x_local_grid, trapz(y_local_grid, f_evaluated.*xi_1));
F_local(2) = trapz(x_local_grid, trapz(y_local_grid, f_evaluated.*xi_2));
F_local(3) = trapz(x_local_grid, trapz(y_local_grid, f_evaluated.*xi_3));
F_local(4) = trapz(x_local_grid, trapz(y_local_grid, f_evaluated.*xi_4));

end