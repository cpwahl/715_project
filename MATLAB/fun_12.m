function out = fun_12(x,y,xL,xR,yD,yU)
out = -((x.^2+xL*xR-x.*(xL+xR)+(y-yU).^2)/((xL-xR).^2*(yD-yU).^2));
end