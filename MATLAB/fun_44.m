function out = fun_44(x,y,xL,xR,yD,yU)
out = ((x-xR).^2+(y-yD).^2)/((xL-xR)^2*(yD-yU)^2);
end