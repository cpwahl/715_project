function out = fun_14(x,y,xL,xR,yD,yU)
out = -(((x-xR).^2+(y-yD).*(y-yU))/((xL-xR).^2*(yD-yU).^2));
end