%Script that will eventually be turned into the local finite element method
clear
%Parameters that are adjustable and might be inputs of the function
%eventually
epsilon = 0.6;
phi_L = 0;
phi_R = 1;
xL = 0;
xR = 1;
yD = 0;
yU = 1;
basis_function_one = 'LRC';
%a = @(x)(cos(x/epsilon)+2);
a= @(x,y)(cos(x.*10).*cos(y.*10));

R=1;
L=0;

switch basis_function_one
    case 'ULC'
        LUC= 1;
        RUC= 0;
        LDC= 0; 
        RDC= 0;
    case 'URC'
        LUC= 0;
        RUC= 1;
        LDC= 0; 
        RDC= 0;
    case 'LLC'
        LUC= 0;
        RUC= 0;
        LDC= 1; 
        RDC= 0;
    case 'LRC'
        LUC= 0;
        RUC= 0;
        LDC= 0; 
        RDC= 1;
end

grid_points = 5;
N = grid_points-2;
delta = (R-L)/(grid_points-1);

x_grid = linspace(xL,xR,grid_points);
y_grid = linspace(yD,yU,grid_points);

lS = cell(grid_points-1,grid_points-1,2);


for j = 1:grid_points-1
    
    
    for m = 1:grid_points-1
        
        %This is all of the integrations of the local matrices and just
        %need to make sure to compose the global matrix correctly based on
        %these loops. 
        %M_local(1,1) = integral2(@(x,y)(a(x,y).*D_linear_lower_L(x,y,x_grid(j),x_grid(j+1), y_grid(j),y_grid(j+1)).D_linear_lower_L(x,y,x_grid(j),x_grid(j+1), y_grid(j),y_grid(j+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        M_local(1,1) = integral2(@(x,y)(a(x,y).*fun_11(x,y, x_grid(j), x_grid(j+1), y_grid(j), y_grid(j+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        M_local(2,2) = integral2(@(x,y)(a(x,y).*fun_22(x,y, x_grid(j), x_grid(j+1), y_grid(j), y_grid(j+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        M_local(3,3) = integral2(@(x,y)(a(x,y).*fun_33(x,y, x_grid(j), x_grid(j+1), y_grid(j), y_grid(j+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        M_local(4,4) = integral2(@(x,y)(a(x,y).*fun_44(x,y, x_grid(j), x_grid(j+1), y_grid(j), y_grid(j+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));

           
        M_local(1,2) = integral2(@(x,y)(a(x,y).*fun_12(x,y, x_grid(j), x_grid(j+1), y_grid(j), y_grid(j+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        M_local(1,3) = integral2(@(x,y)(a(x,y).*fun_13(x,y, x_grid(j), x_grid(j+1), y_grid(j), y_grid(j+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        M_local(1,4) = integral2(@(x,y)(a(x,y).*fun_14(x,y, x_grid(j), x_grid(j+1), y_grid(j), y_grid(j+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));

        M_local(2,3) = integral2(@(x,y)(a(x,y).*fun_23(x,y, x_grid(j), x_grid(j+1), y_grid(j), y_grid(j+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        M_local(2,4) = integral2(@(x,y)(a(x,y).*fun_24(x,y, x_grid(j), x_grid(j+1), y_grid(j), y_grid(j+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        
        M_local(3,4) = integral2(@(x,y)(a(x,y).*fun_34(x,y, x_grid(j), x_grid(j+1), y_grid(j), y_grid(j+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        
        M_local(2,1) = M_local(1,2);
        M_local(3,1) = M_local(1,3);
        M_local(4,1) = M_local(1,4);
        M_local(3,2) = M_local(2,3);
        M_local(4,2) = M_local(2,4);
        M_local(4,3) = M_local(3,4);
        
        lS{j,m,1} = M_local;
        
        
        
        
    end

    
end

 uB = local_basis(@(x)a(x,yU), LUC, RUC, x_grid);
 dB = local_basis(@(x)a(x,yD), LDC, RDC, x_grid);
 lB = local_basis(@(y)a(xL,y), LUC, LDC, y_grid);
 rB = local_basis(@(y)a(xR,y), RDC, RUC, y_grid);
 
coarseA = zeros(N^2,N^2);
coarseF = zeros(N^2,1);

for k=1:N^2
    i = ceil(k/N);
    j = (k-i*N)+N;
    coarseA(k,k) = lS{i,j,1}(2,2)+lS{i,j+1,1}(3,3)...
                    +lS{i+1,j,1}(1,1)+lS{i+1,j+1,1}(4,4);
    if j ~= 1
        coarseA(k,k-1) = lS{i,j,1}(1,2)+lS{i+1,j,1}(3,4);
        if i ~= 1
            coarseA(k,k-N-1) = lS{i,j,1}(2,4);
        end
        if i ~= N
            coarseA(k,k+N-1) = lS{i+1,j,1}(1,3);
        end
    end
    if j ~= N
        coarseA(k,k+1) = lS{i,j+1,1}(1,2)+lS{i+1,j+1,1}(3,4);
        if i ~= 1
            coarseA(k,k-N+1) = lS{i,j+1,1}(1,3);
        end
        if i ~= N
            coarseA(k,k+N+1) = lS{i+1,j+1,1}(2,4);
        end
    end
    if i ~= 1
        coarseA(k,k-N) = lS{i,j,1}(2,3)+lS{i,j+1,1}(1,4);
    end
    if i ~= N
        coarseA(k,k+N) = lS{i+1,j,1}(2,3)+lS{i+1,j+1,1}(1,4);
    end
    
    % need to also consider boundary terms
    if i == 1
        coarseF(k) = coarseF(k) + lS{i,j,1}(2,4)*uB(j)...
                       +lS{i,j,1}(2,3)*uB(j+1)+lS{i,j+1,1}(1,4)*uB(j+1)...
                       +lS{i,j+1,1}(1,3)*uB(j+2);
    elseif i == N
        coarseF(k) = coarseF(k) + lS{i+1,j,1}(1,3)*dB(j)...
                     +lS{i+1,j,1}(2,3)*dB(j+1)+lS{i+1,j+1,1}(1,4)*dB(j+1)...
                     +lS{i+1,j+1,1}(2,4)*dB(j+2);
    end
    if j == 1
        coarseF(k) = coarseF(k) + lS{i,j,1}(2,4)*lB(i)...
                       +lS{i,j,1}(1,2)*lB(i+1)+lS{i+1,j,1}(3,4)*lB(i+1)...
                       +lS{i+1,j,1}(1,3)*lB(i+2);
    elseif j == N
        coarseF(k) = coarseF(k) + lS{i,j+1,1}(1,3)*rB(i)...
                     +lS{i,j+1,1}(1,2)*rB(i+1)+lS{i+1,j+1,1}(3,4)*rB(i+1)...
                     +lS{i+1,j+1,1}(2,4)*rB(i+2);
    end
    if (i == 1) && (j == 1)
        coarseF(k) = coarseF(k) - lS{i,j,1}(2,4)*uB(1);
    elseif (i == 1) && (j == N)
        coarseF(k) = coarseF(k) - lS{i,j+1,1}(1,3)*rB(1);
    elseif (i == N) && (j == 1)
        coarseF(k) = coarseF(k) - lS{i+1,j,1}(1,3)*dB(1);
    elseif (i == N) && (j == N)
        coarseF(k) = coarseF(k) - lS{i+1,j+1,1}(2,4)*uB(N+1);
    end
end


xi = coarseA\coarseF;



