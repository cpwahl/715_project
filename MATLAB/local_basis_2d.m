function [xi_matrix, xi_prime_x, xi_prime_y, x_grid, y_grid] = local_basis_2d(a, xL,xR,yD,yU, grid_points, basis_function_one)

x_grid = linspace(xL,xR,grid_points);
y_grid = linspace(yD,yU,grid_points);

switch basis_function_one
        case 'LLC'
        LUC= 0;
        RUC= 0;
        LDC= 1;
        RDC= 0;
        uB = local_basis(@(x)a(x,yU), RUC, LUC, x_grid);
        dB = local_basis(@(x)a(x,yD), LDC, RDC, x_grid);
        lB = local_basis(@(y)a(xL,y), LDC, LUC, y_grid);
        rB = local_basis(@(y)a(xR,y), RDC, RUC, y_grid);
    case 'LRC'
        LUC= 0;
        RUC= 0;
        LDC= 0;
        RDC= 1;
        uB = local_basis(@(x)a(x,yU), RUC, LUC, x_grid);
        dB = local_basis(@(x)a(x,yD), LDC, RDC, x_grid);
        lB = local_basis(@(y)a(xL,y), LUC, LDC, y_grid);
        rB = local_basis(@(y)a(xR,y), RDC, RUC, y_grid);
    case 'URC'
        LUC= 0;
        RUC= 1;
        LDC= 0;
        RDC= 0;
        uB = local_basis(@(x)a(x,yU), LUC, RUC, x_grid);
        dB = local_basis(@(x)a(x,yD), LDC, RDC, x_grid);
        lB = local_basis(@(y)a(xL,y), LUC, LDC, y_grid);
        rB = local_basis(@(y)a(xR,y), RDC, RUC, y_grid);
    case 'ULC'
        LUC= 1;
        RUC= 0;
        LDC= 0;
        RDC= 0;
        uB = local_basis(@(x)a(x,yU), LUC, RUC, x_grid);
        dB = local_basis(@(x)a(x,yD), RDC, LDC, x_grid);
        lB = local_basis(@(y)a(xL,y), LDC, LUC, y_grid);
        rB = local_basis(@(y)a(xR,y), RUC, RDC, y_grid);
end

N = grid_points-2;
%delta = (xL-xR)/(grid_points-1);



lS = cell(grid_points-1,grid_points-1,2);

xi_matrix = zeros(grid_points);

for j = 1:grid_points-1
    
    
    
    for m = 1:grid_points-1
        
        x_sub_grid = linspace(x_grid(j), x_grid(j+1), 5);
        y_sub_grid = linspace(y_grid(m), y_grid(m+1), 5);
        
        [X_sub, Y_sub] = meshgrid(x_sub_grid, y_sub_grid);
        
        %This is all of the integrations of the local matrices and just
        %need to make sure to compose the global matrix correctly based on
        %these loops.
        %M_local(1,1) = integral2(@(x,y)(a(x,y).*D_linear_lower_L(x,y,x_grid(j),x_grid(j+1), y_grid(j),y_grid(j+1)).D_linear_lower_L(x,y,x_grid(j),x_grid(j+1), y_grid(j),y_grid(j+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        
        %         M_local(1,1) = integral2(@(x,y)(a(x,y).*fun_11(x,y, x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        %         M_local(2,2) = integral2(@(x,y)(a(x,y).*fun_22(x,y, x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        %         M_local(3,3) = integral2(@(x,y)(a(x,y).*fun_33(x,y, x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        %         M_local(4,4) = integral2(@(x,y)(a(x,y).*fun_44(x,y, x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))), x_grid(j), x_grid(j+1),y_grid(m), y_grid(m+1));
        %
        %
        %         M_local(1,2) = integral2(@(x,y)(a(x,y).*fun_12(x,y, x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        %         M_local(1,3) = integral2(@(x,y)(a(x,y).*fun_13(x,y, x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        %         M_local(1,4) = integral2(@(x,y)(a(x,y).*fun_14(x,y, x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        %
        %         M_local(2,3) = integral2(@(x,y)(a(x,y).*fun_23(x,y, x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        %         M_local(2,4) = integral2(@(x,y)(a(x,y).*fun_24(x,y, x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        %
        %         M_local(3,4) = integral2(@(x,y)(a(x,y).*fun_34(x,y, x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))), x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1));
        %
        
        M_local(1,1) = trapz(x_sub_grid, trapz(y_sub_grid, a(X_sub, Y_sub).* fun_11(X_sub, Y_sub,  x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))));
        M_local(2,2) = trapz(x_sub_grid, trapz(y_sub_grid, a(X_sub, Y_sub).* fun_22(X_sub, Y_sub,  x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))));
        M_local(3,3) = trapz(x_sub_grid, trapz(y_sub_grid, a(X_sub, Y_sub).* fun_33(X_sub, Y_sub,  x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))));
        M_local(4,4) = trapz(x_sub_grid, trapz(y_sub_grid, a(X_sub, Y_sub).* fun_44(X_sub, Y_sub,  x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))));
        
        M_local(1,2) = trapz(x_sub_grid, trapz(y_sub_grid, a(X_sub, Y_sub).* fun_12(X_sub, Y_sub,  x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))));
        M_local(1,3) = trapz(x_sub_grid, trapz(y_sub_grid, a(X_sub, Y_sub).* fun_13(X_sub, Y_sub,  x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))));
        M_local(1,4) = trapz(x_sub_grid, trapz(y_sub_grid, a(X_sub, Y_sub).* fun_14(X_sub, Y_sub,  x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))));
        
        M_local(2,3) = trapz(x_sub_grid, trapz(y_sub_grid, a(X_sub, Y_sub).* fun_23(X_sub, Y_sub,  x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))));
        M_local(2,4) = trapz(x_sub_grid, trapz(y_sub_grid, a(X_sub, Y_sub).* fun_24(X_sub, Y_sub,  x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))));
        
        M_local(3,4) = trapz(x_sub_grid, trapz(y_sub_grid, a(X_sub, Y_sub).* fun_34(X_sub, Y_sub,  x_grid(j), x_grid(j+1), y_grid(m), y_grid(m+1))));
        
        
        M_local(2,1) = M_local(1,2);
        M_local(3,1) = M_local(1,3);
        M_local(4,1) = M_local(1,4);
        M_local(3,2) = M_local(2,3);
        M_local(4,2) = M_local(2,4);
        M_local(4,3) = M_local(3,4);
        
        lS{j,m,1} = M_local;
        
        
        
        
    end
    
    
end

coarseA = zeros(N^2,N^2);
coarseF = zeros(N^2,1);

for k=1:N^2
    i = ceil(k/N);
    j = (k-i*N)+N;
    coarseA(k,k) = lS{i,j,1}(2,2)+lS{i,j+1,1}(3,3)...
        +lS{i+1,j,1}(1,1)+lS{i+1,j+1,1}(4,4);
    if j ~= 1
        coarseA(k,k-1) = lS{i,j,1}(1,2)+lS{i+1,j,1}(3,4);
        if i ~= 1
            coarseA(k,k-N-1) = lS{i,j,1}(2,4);
        end
        if i ~= N
            coarseA(k,k+N-1) = lS{i+1,j,1}(1,3);
        end
    end
    if j ~= N
        coarseA(k,k+1) = lS{i,j+1,1}(1,2)+lS{i+1,j+1,1}(3,4);
        if i ~= 1
            coarseA(k,k-N+1) = lS{i,j+1,1}(1,3);
        end
        if i ~= N
            coarseA(k,k+N+1) = lS{i+1,j+1,1}(2,4);
        end
    end
    if i ~= 1
        coarseA(k,k-N) = lS{i,j,1}(2,3)+lS{i,j+1,1}(1,4);
    end
    if i ~= N
        coarseA(k,k+N) = lS{i+1,j,1}(2,3)+lS{i+1,j+1,1}(1,4);
    end
    
%Toms Stuff    
    % need to also consider boundary terms
%     if i == 1
%         coarseF(k) = coarseF(k) + lS{i,j,1}(2,4)*uB(j)...
%             +lS{i,j,1}(2,3)*uB(j+1)+lS{i,j+1,1}(1,4)*uB(j+1)...
%             +lS{i,j+1,1}(1,3)*uB(j+2);
%     elseif i == N
%         coarseF(k) = coarseF(k) + lS{i+1,j,1}(1,3)*dB(j)...
%             +lS{i+1,j,1}(2,3)*dB(j+1)+lS{i+1,j+1,1}(1,4)*dB(j+1)...
%             +lS{i+1,j+1,1}(2,4)*dB(j+2);
%     end
%     if j == 1
%         coarseF(k) = coarseF(k) + lS{i,j,1}(2,4)*lB(i)...
%             +lS{i,j,1}(1,2)*lB(i+1)+lS{i+1,j,1}(3,4)*lB(i+1)...
%             +lS{i+1,j,1}(1,3)*lB(i+2);
%     elseif j == N
%         coarseF(k) = coarseF(k) + lS{i,j+1,1}(1,3)*rB(i)...
%             +lS{i,j+1,1}(1,2)*rB(i+1)+lS{i+1,j+1,1}(3,4)*rB(i+1)...
%             +lS{i+1,j+1,1}(2,4)*rB(i+2);
%     end
%     if (i == 1) && (j == 1)
%         coarseF(k) = coarseF(k) - lS{i,j,1}(2,4)*uB(1);
%     elseif (i == 1) && (j == N)
%         coarseF(k) = coarseF(k) - lS{i,j+1,1}(1,3)*rB(1);
%     elseif (i == N) && (j == 1)
%         coarseF(k) = coarseF(k) - lS{i+1,j,1}(1,3)*dB(1);
%     elseif (i == N) && (j == N)
%         coarseF(k) = coarseF(k) - lS{i+1,j+1,1}(2,4)*uB(N+1);
%     end

%My corrected. All corrections are in the dealings of F in this case. I
%think there was confusion on the indexing and some of the boundaries.
if i == 1
    coarseF(k) = coarseF(k) + lS{i,j,1}(2,4)*dB(j)...
        +lS{i,j,1}(2,3)*dB(j+1)+lS{i,j+1,1}(1,4)*dB(j+1)...
        +lS{i,j+1,1}(1,3)*dB(j+2);
elseif i == N
    coarseF(k) = coarseF(k) + lS{i+1,j,1}(1,3)*uB(j)...
        +lS{i+1,j,1}(2,3)*uB(j+1)+lS{i+1,j+1,1}(1,4)*uB(j+1)...
        +lS{i+1,j+1,1}(2,4)*uB(j+2);
end
if j == 1
    coarseF(k) = coarseF(k) + lS{i,j,1}(2,4)*lB(i)...
        +lS{i,j,1}(1,2)*lB(i+1)+lS{i+1,j,1}(3,4)*lB(i+1)...
        +lS{i+1,j,1}(1,3)*lB(i+2);
elseif j == N
    coarseF(k) = coarseF(k) + lS{i,j+1,1}(1,3)*rB(i)...
        +lS{i,j+1,1}(1,2)*rB(i+1)+lS{i+1,j+1,1}(3,4)*rB(i+1)...
        +lS{i+1,j+1,1}(2,4)*rB(i+2);
end
if (i == 1) && (j == 1)
    coarseF(k) = coarseF(k) - lS{i,j,1}(2,4)*dB(1);
elseif (i == 1) && (j == N)
    coarseF(k) = coarseF(k) - lS{i,j+1,1}(1,3)*rB(1);
elseif (i == N) && (j == 1)
    coarseF(k) = coarseF(k) - lS{i+1,j,1}(1,3)*lB(1);
elseif (i == N) && (j == N)
    coarseF(k) = coarseF(k) - lS{i+1,j+1,1}(2,4)*uB(N+1);
end

end


xi = -coarseA\coarseF;

xi_matrix(2:end-1, 2:end-1) = transpose(reshape(xi ,[N N]));

xi_matrix(:,1)= lB;
xi_matrix(:,end)= rB;
xi_matrix(1, 2:end-1)= dB(2:end-1);
xi_matrix(end, 2:end-1)= uB(2:end-1);

[xi_prime_x, xi_prime_y] = gradient(xi_matrix); 


end
