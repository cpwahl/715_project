function out = linear_R_plotting(x,xl,xr)
if x<=xr && x>=xl
    out = (x-xr)/(xl-xr);
else
    out =0;
end
end
