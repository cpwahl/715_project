function out = fun_24(x,y,xL,xR,yD,yU)
out = (x.^2+xL*xR-x.*(xL+xR)+(y-yD).*(y-yU))/((xL-xR)^2*(yD-yU)^2);
end