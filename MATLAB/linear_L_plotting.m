function out = linear_L_plotting(x,xl,xr)
if x<=xr && x>=xl
    out = (x-xl)/(xr-xl);;
else
    out =0;
end
end