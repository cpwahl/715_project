function out = D_linear_lower_R(x, y, xL, xR, yL, yR)
    out = [(y - yR)/((xL - xR)*(-yL + yR)); (x - xL)/((xL - xR)*(-yL + yR))];
end