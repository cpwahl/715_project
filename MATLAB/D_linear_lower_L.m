function out = D_linear_lower_L(x, y, xL, xR, yL, yR)
    out = [(y - yR)/((xL - xR)*(yL - yR)); (x - xR)/((xL - xR)*(yL - yR))];
end