function out = linear_L(x,xl,xr)
    out = (x-xl)/(xr-xl);
end