function out = fun_11(x,y,xL,xR,yD,yU)
out = ((x - xR).^2 + (y - yU).^2)/((xL - xR).^2*(yD - yU).^2);
end
