function out = linear_R(x,xl,xr)
    out = (x-xr)/(xl-xr);
end