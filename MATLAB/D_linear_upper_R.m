function out = D_linear_upper_R(x, y, xL, xR, yL, yR)
    out = [(y - yL)/((xL - xR)*(yL - yR)); (x - xL)/((xL - xR)*(yL - yR))];
end