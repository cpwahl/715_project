clear
M=16;
N = [3,7,11, 16, 32];
err = NaN([1 length(N)]);

for j =1:length(N)
    j
    err(1,j)=MsFEM_func(M, N(j)); 
end

plot(log(N), log(err));
polyfit(log(N), log(err), 1)