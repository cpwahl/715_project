function out = fun_22(x,y,xL,xR,yD,yU)
out = ((x-xL).^2+(y-yU).^2)/((xL-xR)^2*(yD-yU)^2);
end
