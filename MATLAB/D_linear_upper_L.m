function out = D_linear_upper_L(x, y, xL, xR, yL, yR)
    out = [(y - yL)/((-xL + xR)*(yL - yR)); (x - xR)/((-xL + xR)*(yL - yR))];
end