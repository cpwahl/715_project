function out = fun_23(x,y,xL,xR,yD,yU)
out = -(((x-xL).^2+(y-yD).*(y-yU))/((xL-xR)^2*(yD-yU)^2));
end