function [Stiff_local, Load_local] = local_stiff_load_multi_run(a, f, x_L, x_R, M, xi_L, xi_R)

% This script uses the local_basis FEM function and finds the local
% stiffness matrix for the larger K-cells defined by x_L and x_R.

%Input is a(x), f(x) and x_L and x_R of each cell.

%Output is the stiffness matrix [<a psi_1', psi_1'>, <a psi_1', psi_2'>; <a psi_2', psi_1'>, <a psi_2', psi_2'>].
%psi_1 takes the value 1 at the left and psi_2 taking the value of 1 at the
%right.
%The load matrix has output [<psi_1, f>; <psi_2, f>]

x_grid_points = M+1;

delta_x = (x_L - x_R)/x_grid_points;
x_grid = linspace(x_L, x_R, x_grid_points);
a_evaluated = a(transpose(x_grid));
f_evaluated = f(transpose(x_grid));

%xi = local_basis(a, phi_L, phi_R, x_L, x_R, grid_points)




%Computes the centered difference except for the left and right end points
%which use left and right sided derivatives. This could be causing some
%problems and first order accuracy but I am not sure what the best way to
%fix this would be.
xi_L_prime = 1/(2*delta_x).*(xi_L(1:end-2)-xi_L(3:end)); 
xi_R_prime = 1/(2*delta_x).*(xi_R(1:end-2)-xi_R(3:end));
% xi_L_prime = [ 1/delta_x.*(xi_L(1) - xi_L(2)); xi_L_prime; 1/delta_x.*(xi_L(end-1) - xi_L(end))];
% xi_R_prime = [ 1/delta_x.*(xi_R(1) - xi_R(2)); xi_R_prime; 1/delta_x.*(xi_R(end-1) - xi_R(end))];
%Using left and right sided derivatives substantially decreased the rate of
%convergence so I just did the analytic derivative approaching from the
%left and right which helped a lot.
xi_L_prime = [ -delta_x.*(xi_L(1)); xi_L_prime; -delta_x.*xi_L(end)];
xi_R_prime = [ delta_x.*xi_R(1) ; xi_R_prime; delta_x.*xi_R(end)];


Stiff_local = NaN([2 2]);

Stiff_local(1,1) = trapz(x_grid, a_evaluated.*xi_L_prime.*xi_L_prime);
Stiff_local(1,2) = trapz(x_grid, a_evaluated.*xi_R_prime.*xi_L_prime);
Stiff_local(2,1) = Stiff_local(1,2);
Stiff_local(2,2) = trapz(x_grid, a_evaluated.*xi_R_prime.*xi_R_prime);

Load_local(1, :) = trapz(x_grid, f_evaluated.*xi_L);
Load_local(2, :) = trapz(x_grid, f_evaluated.*xi_R);


end
