clear
N = 11;
M = [1,3,5,7,9];

for j =1:length(M)
    err(j)=MsFEM_func(M(j), N); 
end

plot(err);
hold on
plot((M+1).^2 .* (N+1)^2)